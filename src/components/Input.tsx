import React from "react";
import styled from "styled-components";

const StyledInput = styled.input`
  height: 30px;
  width: 50vw;
  font-size: 16px;
  border: 1px solid black;
  padding-left: 5px;
`;

export default function Input(
  props: React.InputHTMLAttributes<HTMLInputElement>
) {
  return <StyledInput {...props}></StyledInput>;
}
