import React, { useState } from "react";
import styled from "styled-components";
import { IProgramm } from "../pages/Home";
import Button from "./Button";
import Input from "./Input";

interface IProps {
  id: number;
  handleDelete: () => void;
  onInputChange: (
    el: number,
    data: {
      title?: string;
      description?: string;
      linkName?: string;
      image?: string;
    }
  ) => void;
  initData: IProgramm;
}
const Container = styled.div`
  display: flex;
  flex-direction: column;
  border: 2px solid black;
  padding: 30px 30px 0 30px;
  margin: 15px 0;
`;

const Form = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const Col = styled.div`
  flex: 1;
`;
const Row = styled.div<{ left?: boolean }>`
  display: flex;
  margin: 15px 0;
  justify-content: ${props => (props.left ? "flex-end" : "flex-start")};
`;

const Label = styled.div`
  margin: 15px 0 5px 0;
  font-size: 16px;
  font-weight: bold;
`;

const Textarea = styled.textarea`
  width: 50vw;
  height: 60px;
  font-size: 16px;
  border: 1px solid black;
  padding-left: 5px;
`;

const Image = styled.img`
  width: 200px;
  object-fit: contain;
`;

export default function ProgrammInput(props: IProps) {
  const [image, setImage] = useState<string>("");
  return (
    <Container>
      <Form>
        <Col>
          <Label>Title</Label>
          <Input
            defaultValue={props.initData.title}
            placeholder="Programm Title..."
            onChange={e =>
              props.onInputChange(props.id, { title: e.target.value })
            }
          ></Input>

          <Label>Beschreibung</Label>
          <Textarea
            placeholder="Programm Beschreibung..."
            defaultValue={props.initData.description}
            onChange={e =>
              props.onInputChange(props.id, { description: e.target.value })
            }
          ></Textarea>

          <Label>Link Name</Label>
          <Input
            placeholder="Link Such-Name..."
            defaultValue={props.initData.linkName}
            onChange={e =>
              props.onInputChange(props.id, { linkName: e.target.value })
            }
          ></Input>

          <Label>Bild URL</Label>
          <Input
            defaultValue={props.initData.image}
            placeholder="Bild URL..."
            onChange={e => {
              props.onInputChange(props.id, { image: e.target.value });
              setImage(e.target.value);
            }}
          ></Input>
        </Col>
        <Col
          style={{
            marginTop: 30,
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <Image src={image}></Image>
        </Col>
      </Form>
      <Row left>
        <Button small danger onClick={props.handleDelete}>
          Programm löschen
        </Button>
      </Row>
    </Container>
  );
}
