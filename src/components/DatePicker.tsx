import React from "react";
import styled from "styled-components";

const DateInput = styled.input`
  border: 2px solid black;
  margin-right: 10px;
  margin-bottom: 10px;
  height: 30px;
  font-size: 16px;
`;

export default function DatePicker(props: {
  date: string;
  onChange: (e: string) => void;
}) {
  return (
    <DateInput
      type="date"
      value={props.date}
      onChange={e => props.onChange(e.target.value)}
    ></DateInput>
  );
}
