import React, { ReactNode } from "react";
import styled from "styled-components";

const StyledButton = styled.button<IProps>`
  cursor: pointer;
  height: ${props => (props.small ? "30px" : "40px")};
  border: 2px solid black;
  font-size: ${props => (props.small ? "10px" : "16px")};
  font-weight: bold;
  &:hover {
    background: ${props => (props.danger ? "#ff726f" : "#b7df89")};
  }
  &:focus {
    outline: 0;
  }
`;

interface IProps {
  onClick: () => void;
  children: ReactNode;
  small?: boolean;
  danger?: boolean;
}
export default function Button(props: IProps) {
  return <StyledButton {...props} small={props.small}></StyledButton>;
}
