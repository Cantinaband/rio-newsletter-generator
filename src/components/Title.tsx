import React, { ReactNode } from "react";
import styled from "styled-components";
interface IProps {
  children: ReactNode;
}

const H2 = styled.h2``;
export default function Title(props: IProps) {
  return <H2>{props.children}</H2>;
}
