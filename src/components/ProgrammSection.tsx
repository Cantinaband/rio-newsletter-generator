import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { IProgramm } from "../pages/Home";
import ProgrammInput from "./ProgrammInput";
import Button from "./Button";
import Input from "./Input";

interface IProps {
  deleteSection: () => void;
  onSectionChange: (data: { title: string; programms: IProgramm[] }) => void;
  initTitle: string;
  initProgramms: IProgramm[];
}
const Section = styled.div`
  border: 3px solid black;
  padding: 0 30px 0px 30px;
  margin: 15px 0;
`;

const Row = styled.div<{ left?: boolean }>`
  display: flex;
  margin: 15px 0;
  justify-content: ${props => (props.left ? "flex-end" : "flex-start")};
`;

export default function ProgrammSection(props: IProps) {
  const [title, setTitle] = useState("");
  const [programms, addProgramm] = useState<IProgramm[]>([]);

  useEffect(() => {
    setTitle(props.initTitle);
    addProgramm(props.initProgramms);
  }, [props.initTitle, props.initProgramms]);

  const delteProgramm = (el: number) => {
    let oldProgramms = programms;
    let newProgramms = oldProgramms.filter(e => e.el !== el);
    addProgramm(newProgramms);
  };

  const handleInputChange = (
    el: number,
    data: { title?: string; description?: string }
  ) => {
    let newProgramms = programms;
    Object.assign(newProgramms[el - 1], data);
    addProgramm(newProgramms);
    props.onSectionChange({ title: title, programms: programms });
  };
  return (
    <Section>
      <h3>Section Title</h3>
      <Row>
        <Input
          style={{ borderWidth: 2 }}
          placeholder="Section Title..."
          value={title}
          type="test"
          onChange={e => setTitle(e.target.value)}
        ></Input>
      </Row>

      {programms.map(e => (
        <ProgrammInput
          key={e.el}
          id={e.el}
          initData={e}
          handleDelete={() => delteProgramm(e.el)}
          onInputChange={(el, data) => handleInputChange(el, data)}
        ></ProgrammInput>
      ))}
      <Button
        small
        onClick={() =>
          addProgramm([...programms, { el: programms.length + 1 }])
        }
      >
        Programm hinzufügen
      </Button>
      <Row left>
        <Button small danger onClick={props.deleteSection}>
          Section löschen
        </Button>
      </Row>
    </Section>
  );
}
