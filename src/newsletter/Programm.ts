import { IProgramm } from "../pages/Home";

function getProgramm(props: IProgramm) {
  const programm = `<table
align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
>
<tr>
 <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
<![endif]-->
<div style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;">
<table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;">
<tbody>
 <tr>
   <td style="direction:${
     props.el % 2 === 0 ? "rtl" : "ltr"
   };font-size:0px;padding:20px 0;padding-bottom:0px;text-align:center;">
     <!--[if mso | IE]>
         <table role="presentation" border="0" cellpadding="0" cellspacing="0">
       
<tr>

   <td
      class="" style="vertical-align:top;width:300px;"
   >
 <![endif]-->
     <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
       <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
         <tr>
           <td align="center" style="font-size:0px;padding:10px 25px;padding-top:0px;padding-right:30px;padding-bottom:20px;padding-left:30px;word-break:break-word;">
             <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
               <tbody>
                 <tr>
                   <td style="width:240px;"> <a href="https://www.kinoheld.de/kino-muenchen/rio-filmpalast-muenchen?mode=widget&target=self&rb=1&layout=movies&showName=${
                     props.linkName
                   }" target="_blank">
 
<img
alt="" height="auto" src="${
    props.image
  }" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="240"
/>

</a> </td>
                 </tr>
               </tbody>
             </table>
           </td>
         </tr>
       </table>
     </div>
     <!--[if mso | IE]>
   </td>
 
   <td
      class="" style="vertical-align:top;width:300px;"
   >
 <![endif]-->
     <div class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
       <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="vertical-align:top;" width="100%">
         <tr>
           <td align="left" style="font-size:0px;padding:10px 25px;padding-top:0px;padding-right:40px;padding-bottom:0px;padding-left:40px;word-break:break-word;">
             <div style="font-family:Arial, sans-serif;font-size:13px;line-height:22px;text-align:left;color:#55575d;">
               <p style="margin: 10px 0; color:#151e23; font-size:16px; font-family:Georgia,Helvetica,Arial,sans-serif"> <b>${
                 props.title
               }</b> </p>
               <p style="line-height: 16px; margin: 10px 0;font-size:14px; color:#151e23; font-family:Georgia,Helvetica,Arial,sans-serif; color:#354552">${
                 props.description
               }</p> <a style="line-height: 16px; margin: 10px 0; color:#354552; font-size:14px; font-family:Georgia,Helvetica,Arial,sans-serif"
                 href="https://www.kinoheld.de/kino-muenchen/rio-filmpalast-muenchen?mode=widget&target=self&rb=1&layout=movies&showName=${
                   props.linkName
                 }" alt="">
   <u>Zum Ticket</u>
 </a></div>
           </td>
         </tr>
       </table>
     </div>
     <!--[if mso | IE]>
   </td>
 
</tr>

         </table>
       <![endif]-->
   </td>
 </tr>
</tbody>
</table>
</div>
<!--[if mso | IE]>
 </td>
</tr>
</table>
`;

  return programm;
}
export default getProgramm;
