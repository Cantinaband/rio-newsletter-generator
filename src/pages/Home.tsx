import React, { useState, useRef } from "react";
import styled from "styled-components";
import DatePicker from "../components/DatePicker";
import Title from "../components/Title";
import getNewsletter from "../newsletter/Newsletter";
import ProgrammSection from "../components/ProgrammSection";
import Button from "../components/Button";

const StyledHome = styled.div`
  display: flex;
  flex-direction: column;
  padding: 30px 30px 0 30px;
  font-family: Arial, Helvetica, sans-serif;
`;

const Logo = styled.img`
  height: 70px;
  object-fit: contain;
  margin-bottom: 30px;
`

const Iframe = styled.iframe<{ calculatedHeight: number }>`
  margin: 30px 0;
  border: ${props => props.calculatedHeight > 0 && "3px solid black"};
  height: ${props => props.calculatedHeight}px;
`;

const Row = styled.div`
  margin: 30px 0;
`;

const DownloadLink = styled.a`
  border: 2px solid black;
  line-height: 34px;
  font-size: 16px;
  text-decoration: none;
  color: #000;
  margin: 30px 0;
  text-align: center;
  font-weight: bold;
  &:hover {
    background: #ffc65a;
  }
  &:focus {
    outline: 0;
  }
`;

const Upload = styled.label`
  border: 2px solid black;
  font-size: 20px;
  line-height: 30px;
  text-align: center;
  font-weight: bold;
  &:hover {
    background: #b7df89;
  }
`;

export interface IProgramm {
  el: number;
  title?: string;
  description?: string;
  linkName?: string;
  image?: string;
}

export interface ISection {
  id: number;
  title: string;
  programms: IProgramm[];
}

export default function Home() {
  const [newsletterWeek, setNewsletterWeek] = useState<{
    from: string;
    to: string;
  }>({from: '', to: ''});
  const [sections, addSection] = useState<ISection[]>([]);
  const [newsletter, setNewsletter] = useState<string>("");
  const [iframeHeight, setIframeHeight] = useState(0);
  const iFrame = useRef<any>();

  const generateNewsletter = () => {
    const newNewsletter = getNewsletter(sections, newsletterWeek);
    setNewsletter(newNewsletter);
  };

  const deleteSection = (id: number) => {
    let oldSections = sections;
    let newSections = oldSections.filter(e => e.id !== id);
    addSection(newSections);
  };

  const handleSectionChange = (
    id: number,
    data: { title: string; programms: IProgramm[] }
  ) => {
    let newSections = sections;
    Object.assign(newSections[id - 1], data);
    addSection(newSections);
  };


  return (
    <StyledHome>
      <Logo src="https://www.riopalast.de/wp-content/uploads/2019/12/logo-kaum-rand.jpg" />
      <Upload
        htmlFor="file-upload"
        style={{
          border: "2px solid #000",
          display: "inline-block",
          padding: "6px 12px",
          cursor: "pointer"
        }}
      >
        <i></i> Newsletter hochladen
      </Upload>
      <input
        style={{ display: "none" }}
        id="file-upload"
        type="file"
        accept=".json"
        onChange={e => {
          var file =
            e.target &&
            e.target.files &&
            e.target.files[0] &&
            e.target.files[0];
          var reader = new FileReader();
          reader.onload = function(e) {
            if (e.target) {
              const data = JSON.parse((e.target.result as unknown) as string);
              addSection(data.sections);
              setNewsletterWeek(data.week)
            }
          };

          file && reader.readAsText(file);
        }}
      ></input>
      <Row>
        <Title>Programmwoche</Title>
        <DatePicker key={'dateFrom'} onChange={(value) => setNewsletterWeek({to: newsletterWeek.to,from: value})} date={newsletterWeek.from}></DatePicker>
        <DatePicker key={'dateTo'} onChange={(value) => setNewsletterWeek({from: newsletterWeek.from,to: value})} date={newsletterWeek.to}></DatePicker>
      </Row>

      {sections.map((e, i) => (
        <ProgrammSection
          key={i}
          deleteSection={() => deleteSection(e.id)}
          onSectionChange={data => handleSectionChange(e.id, data)}
          initTitle={e.title}
          initProgramms={e.programms}
        ></ProgrammSection>
      ))}
      <Row>
        <Button
          onClick={() =>
            addSection([
              ...sections,
              { id: sections.length + 1, programms: [], title: "" }
            ])
          }
        >
          Section hinzufügen
        </Button>
      </Row>
      <Button onClick={generateNewsletter}>Newsletter generieren</Button>

      <Iframe
        ref={iFrame}
        calculatedHeight={iframeHeight}
        onLoad={() =>
          iFrame.current &&
          iFrame.current.contentWindow &&
          setIframeHeight(
            iFrame.current.contentWindow.document.body.scrollHeight
          )
        }
        frameBorder="0"
        srcDoc={newsletter}
      ></Iframe>
      <DownloadLink
          href={
            "data:text/html;charset=utf-8," +
            encodeURIComponent(newsletter)
          }
          download="newsletter.html"
        >
          HTML download 
        </DownloadLink> 
      {sections.length > 0 && (
        <DownloadLink
          href={
            "data:text/json;charset=utf-8," +
            encodeURIComponent(JSON.stringify({week: {from: newsletterWeek?.from, to: newsletterWeek?.to},sections: sections}))
          }
          download="data.json"
        >
          Newsletter speichern
        </DownloadLink>
      )}
    </StyledHome>
  );
}
